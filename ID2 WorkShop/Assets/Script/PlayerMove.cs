﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    [Range(1,20)][SerializeField] private int moveSpeed;
    [Range(5,30)][SerializeField] private int jumpHigh;

    public bool isGrounded;


    private Rigidbody2D rb;
    private SpriteRenderer sr;
    private Animator anim;


    private bool pressedJump;
    public float dashSpeed;
    private float dashTime;
    public float startDashTime;
    private int direction;
    



    private void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        sr = this.GetComponent<SpriteRenderer>();
        dashTime = startDashTime;
        anim = this.GetComponent<Animator>();

    }
    //  Jump input checking
    private void Update()
    {
        if (Input.GetButtonDown("Jump") && isGrounded == true)
        {
            pressedJump = true;
            anim.SetBool("Jump", true);


        }

    }
 
    private void FixedUpdate()
    {   // Movement
        rb.velocity = new Vector2(Input.GetAxisRaw("Horizontal")* moveSpeed, rb.velocity.y);
        anim.SetFloat("MoveSpeed", Mathf.Abs(Input.GetAxisRaw("Horizontal")));


        // Move Left&Right
        if (Input.GetAxisRaw("Horizontal") > 0)
        {
            sr.flipX = false;
        }
        else if (Input.GetAxisRaw("Horizontal") < 0)
        {
            sr.flipX = true;
        }


        // Jump
        if (pressedJump == true)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpHigh);
            pressedJump = false;

        }

        else if(isGrounded = true)
        {
            anim.SetBool("Jump", false);
        }
       

         {
        if(direction == 0){
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                direction = 1;
            }

            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                direction = 2;
            }

            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                direction = 3;
            }

            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                direction = 4;
            }
        }else 
        {
            if(dashTime <= 0)
            {
                direction = 0;
                dashTime = startDashTime;
                rb.velocity = Vector2.zero;
            }

            else
            {
                dashTime -= Time.deltaTime;
            }

            if(direction == 1)
            {
                rb.velocity = Vector2.left * dashSpeed;
            }

            else if (direction == 2)
            {
                rb.velocity = Vector2.right * dashSpeed;
            }

            else if (direction == 3)
            {
                rb.velocity = Vector2.up * dashSpeed;
            }

            else if (direction == 4)
            {
                rb.velocity = Vector2.down * dashSpeed;
            }

        }
}

    }

    
   
}
