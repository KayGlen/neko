﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGroundCheck : MonoBehaviour
{
    private PlayerMove player;

    private void Start()
    {
        player = this.transform.parent.GetComponent<PlayerMove>();
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Ground"))
        {
            print("hi");
            player.isGrounded = true;

        }

    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.CompareTag("Ground"))
        {
            print("Bye");
            player.isGrounded = false;
            
        }

    }
}
