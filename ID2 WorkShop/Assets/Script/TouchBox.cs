﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchBox : MonoBehaviour
{
    [SerializeField] private AudioSource src;
    [SerializeField] private AudioClip sfx;

   private void OnCollisionEnter2D(Collision2D other)
   {
       src.PlayOneShot(sfx);
   }
}
